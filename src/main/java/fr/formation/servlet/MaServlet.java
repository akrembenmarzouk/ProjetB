package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.formation.beans.MonBean;

public class MaServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		//envoie dun javaBean
		
		MonBean monBean = new MonBean();
		req.setAttribute( "unBean", monBean );
		
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/index.jsp" ).forward( req, resp );
	}
}
